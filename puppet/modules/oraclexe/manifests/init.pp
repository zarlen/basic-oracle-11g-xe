class oraclexe::server {

  file {'/etc/init/mounted-dev.conf':
    source => "puppet:///modules/oraclexe/mounted-dev.conf",
    owner => 'root',
    group => 'root',
  }

  exec {'restartmounteddev':
    command => '/sbin/start mounted-dev',
    require => File['/etc/init/mounted-dev.conf'],
    onlyif => "/bin/uname -a | grep -c 'precise'",
  }

  package {
    "bc":
        ensure => installed;
    "ntp":
        ensure => installed;
    "htop":
        ensure => installed;
    "monit":
        ensure => installed;
    "rsyslog":
        ensure => installed;
    "curl":
        ensure => installed;
    "libaio1":
        ensure => installed;
    "unixodbc":
        ensure => installed;
  }
  service {
    "ntp":
        ensure => stopped,
		require => Package['ntp'];
    "monit":
        ensure => running,
		require => Package['monit'];
/*      subscribe => File["/etc/monit/conf.d/monitrc"];*/
    "rsyslog":
        ensure => running,
		require => Package['rsyslog'];
    "procps":
        ensure => running;
  }
 
    file {
        "/etc/sysctl.d/60-oracle.conf" :
            source => "puppet:///modules/oraclexe/xe-sysctl.conf";
    }
    user { "syslog":
        ensure => present,
        groups => ["syslog","adm"];
    }
}
 
class oraclexe::xe{
	
    file {
        "/tmp/oracle-xe_11.2.0-2_amd64.deb":,
            source => "puppet:///modules/oraclexe/oracle-xe_11.2.0-2_amd64.deb";
        "/tmp/xe.rsp":
            source => "puppet:///modules/oraclexe/xe.rsp";
        "/etc/init.d/oracle-shm":
            mode => 0755,
            source => "puppet:///modules/oraclexe/oracle-shm";
    }
    exec {
        "configure xe":
            command => "/etc/init.d/oracle-xe configure responseFile=/tmp/xe.rsp >> /tmp/xe-install.log",
            require => [Package["oracle-xe"],Exec["update-rc oracle-shm"], Exec['restartmounteddev']],
			timeout => 1200,
			creates => '/tmp/xe-install.log',
            user => root;
        "update-rc oracle-shm":
            command => "/usr/sbin/update-rc.d oracle-shm defaults 01 99",
            cwd => "/etc/init.d",
            require => File["/etc/init.d/oracle-shm"],
			timeout => 1200,
            user => root;
		"update-rc oracle-xe":
            command => "/usr/sbin/update-rc.d oracle-xe defaults 01 99",
            cwd => "/etc/init.d",
            require => Exec["configure xe"],
			timeout => 1200,
            user => root;
        "oracle-shm":
            command => "/etc/init.d/oracle-shm start",
            user => root,
			timeout => 1200,
            require => File["/etc/init.d/oracle-shm"];
    }
    package {
        "oracle-xe":
            provider => "dpkg",
            ensure => installed,
            require => [
				File["/tmp/oracle-xe_11.2.0-2_amd64.deb"],
				Package["bc"],
				Package["ntp"],
				Package["monit"], 
				Package["htop"],
				Package["rsyslog"],
				Package["libaio1"],
				Package["curl"],
				Package["unixodbc"],
			],
            source => "/tmp/oracle-xe_11.2.0-2_amd64.deb";
    }
}
