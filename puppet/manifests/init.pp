group{'puppet':
	ensure => present,
}
exec{'change mirrors':
	command => "/bin/sed -i 's/gb.archive.ubuntu.com/mirror.anl.gov\/pub/g' /etc/apt/sources.list && /usr/bin/apt-get update",
}
# Ensure apt-get update has been run before installing any packages
Exec["change mirrors"] -> Package <| |>



class { 'oraclexe::server': }  -> class { 'oraclexe::xe': }

file {'/u01':
	ensure => directory,
	owner => root,
	group => root,
	mode => 664	
}

file {'/u01/properties':
	ensure => directory,
	mode => 666,
	require => File['/u01'],
}


